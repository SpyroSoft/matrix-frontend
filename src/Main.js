import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './components/Home'
import About from './components/About'
import Competency from './components/Competency'
import Routing from './Routing'

const Main = () => (
  <main>
    <Routing />
  </main>
)

export default Main
