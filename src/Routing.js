import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './components/Home'
import About from './components/About'
import Competency from './components/Competency'
import { BrowserRouter } from 'react-router-dom'
 
const Routing = () => (

    <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/about' component={About}/>
        <Route path='/competency' component={Competency}/>
    </Switch>
 
)
 
export default Routing
 