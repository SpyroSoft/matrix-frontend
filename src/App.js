import React from 'react';
import './App.css';
import Main from './Main';
import Header from './Header';

const App = () => (
  <div>
    <Header />
    <Main />
  </div>
)

export default App
